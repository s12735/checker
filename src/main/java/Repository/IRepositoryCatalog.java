package Repository;

import domain.Person;
import domain.User;

public interface IRepositoryCatalog {
	
	public IRepository<Person> getPerson();

	public IRepository<User> getUser();

	public void commit();
}
