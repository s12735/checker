package Repository.Imp;

import java.sql.ResultSet;
import java.sql.SQLException;

import Repository.IEntityBuilder;
import domain.Person;

public class PersonBuilder implements IEntityBuilder<Person>{

	public Person build(ResultSet rs) throws SQLException {
		Person person = new Person();
		person.setFirstName(rs.getString("name"));
		person.setSurname(rs.getString("surname"));
		person.setPesel(rs.getString("pesel"));
		person.setNip(rs.getString("nip"));
		person.setEmail(rs.getString("email"));
		person.setDateOfBirth(rs.getDate("date"));
		person.setId(rs.getInt("id"));
		return person;
	}

}
