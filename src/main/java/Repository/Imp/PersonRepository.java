package Repository.Imp;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;

import Repository.IEntityBuilder;
import Repository.IRepository;
import UnityOfWork.IUnitOfWork;
import domain.Person;

public class PersonRepository extends Repository<Person> implements IRepository<Person> {

	public PersonRepository(Connection connection, IEntityBuilder<Person> builder, IUnitOfWork uow) {
		super(connection,builder, uow);
	}

	@Override
	protected void setUpUpdateQuery(Person entity) throws SQLException {
		update.setString(1, entity.getFirstName());
		update.setString(2, entity.getSurname());
		update.setString(3, entity.getPesel());
		update.setString(4, entity.getNip());
		update.setString(5, entity.getEmail());
		update.setDate(6, (Date) entity.getDateOfBirth());
		update.setInt(7, entity.getId());
	}

	@Override
	protected void setUpInsertQuery(Person entity) throws SQLException {
		insert.setString(1, entity.getFirstName());
		insert.setString(2, entity.getSurname());
		insert.setString(3, entity.getPesel());
		insert.setString(4, entity.getNip());
		insert.setString(5, entity.getEmail());
		insert.setDate(6, (Date) entity.getDateOfBirth());
	}

	@Override
	protected String getTableName() {
		return "Persons";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE Persons SET (name, surname, pesel, nip, email, date)=(?,?) WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO Persons(name, surname, pesel, nip, email, date)"
				+ "VALUES(?,?)";
	}

}