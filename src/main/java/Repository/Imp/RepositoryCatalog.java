package Repository.Imp;

import java.sql.Connection;

import Repository.IRepository;
import Repository.IRepositoryCatalog;
import UnityOfWork.IUnitOfWork;
import domain.Person;
import domain.User;

public class RepositoryCatalog implements IRepositoryCatalog {
	
	private Connection connection;
	private IUnitOfWork uow;
	
	public RepositoryCatalog(Connection connection, IUnitOfWork uwo) {
		this.connection = connection;
		this.uow = uwo;
	}
	public IRepository<Person> getPerson() {
		return new PersonRepository(connection, new PersonBuilder(), uow);
	}

	public IRepository<User> getUser() {
		return new UserRepository(connection, new UserBuilder(), uow);
	}

	public void commit() {
		uow.commit();
	}

}
		