package Repository.Imp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import Repository.IRepositoryCatalog;
import UnityOfWork.IUnitOfWork;
import UnityOfWork.UnitOfWork;

public class RepositoryCatalogProvider {
	private static String url = "jdbc:hsqldb:hsql://localhost";

	public static IRepositoryCatalog catalog() {

		try {
			Connection connection = DriverManager.getConnection(url);
			IUnitOfWork uow = new UnitOfWork(connection);
			IRepositoryCatalog catalog = new RepositoryCatalog(connection, uow);

			return catalog;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}
