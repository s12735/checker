package Repository.Imp;

import java.sql.Connection;
import java.sql.SQLException;

import Repository.IEntityBuilder;
import Repository.IRepository;
import UnityOfWork.IUnitOfWork;
import domain.User;

public class UserRepository extends Repository<User> implements IRepository<User> {

	public UserRepository(Connection connection, IEntityBuilder<User> builder, IUnitOfWork uow) {
		super(connection,builder, uow);
	}

	@Override
	protected void setUpUpdateQuery(User entity) throws SQLException {
		update.setString(1, entity.getLogin());
		update.setString(2, entity.getPassword());
		update.setInt(3, entity.getId());
	}

	@Override
	protected void setUpInsertQuery(User entity) throws SQLException {
		insert.setString(1, entity.getLogin());
		insert.setString(2, entity.getPassword());
	}

	@Override
	protected String getTableName() {
		return "users";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE users SET (login,password)=(?,?) WHERE id=?";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO users(login,password)"
				+ "VALUES(?,?)";
	}

}