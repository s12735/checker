package checker.rules;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class DateOfBirthRule implements ICheckRule<Person> {

	private byte PESEL[] = new byte[6];
	int peselYear, peselMonth, peselDay;
	int year, month, day;

	SimpleDateFormat df = new SimpleDateFormat("dd/MM/yy");
	Date data = new Date();

	public CheckResult checkRule(Person entity) {

		getDateFromPerson(entity);
		getDateFromPesel(entity.getPesel());
	
		if (peselDay != day || month != peselMonth || year != peselYear) {
			return new CheckResult("", RuleResult.Error);
		}

		return new CheckResult("", RuleResult.Ok);
	}

	public void getDateFromPerson(Person entity) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(entity.getDateOfBirth());
		year = cal.get(Calendar.YEAR);
		month = cal.get(Calendar.MONTH) + 1;
		day = cal.get(Calendar.DAY_OF_MONTH);

	}

	public void getDateFromPesel(String pesel) {
		for (int i = 0; i < 6; i++) {
			PESEL[i] = Byte.parseByte(pesel.substring(i, i + 1));
		}
		peselYear = getBirthYear();
		peselMonth = getBirthMonth();
		peselDay = getBirthDay();

	}

	public int getBirthYear() {
		int year;
		int month;
		year = 10 * PESEL[0];
		year += PESEL[1];
		month = 10 * PESEL[2];
		month += PESEL[3];
		if (month > 80 && month < 93) {
			year += 1800;
		} else if (month > 0 && month < 13) {
			year += 1900;
		} else if (month > 20 && month < 33) {
			year += 2000;
		} else if (month > 40 && month < 53) {
			year += 2100;
		} else if (month > 60 && month < 73) {
			year += 2200;
		}
		return year;
	}

	public int getBirthMonth() {
		int month;
		month = 10 * PESEL[2];
		month += PESEL[3];
		if (month > 80 && month < 93) {
			month -= 80;
		} else if (month > 20 && month < 33) {
			month -= 20;
		} else if (month > 40 && month < 53) {
			month -= 40;
		} else if (month > 60 && month < 73) {
			month -= 60;
		}
		return month;
	}

	public int getBirthDay() {
		int day;
		day = 10 * PESEL[4];
		day += PESEL[5];
		return day;
	}
}
