package checker.rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;

public class EmailRule implements ICheckRule<Person> {
	private Pattern pattern;
	private Matcher matcher;
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	// email starts with A-Z a-z 0-9 (1 or more char)
	// optionally followed by .  and A-Z a-z 0-9
	// then have to be @
	// optionally followed by . and A-Z a-z 0-9
	// followed by . and A-Z a-z 0-9 (at least 2 charters)
	
	
	public CheckResult checkRule(Person entity) {
		if(Validate(entity.getEmail()) == false){
			return new CheckResult("", RuleResult.Error);
		}
		return new CheckResult("", RuleResult.Ok);
	}
	
	private boolean Validate(String email){
		pattern = Pattern.compile(EMAIL_PATTERN);
		matcher = pattern.matcher(email);
		return matcher.matches();
	}
}
