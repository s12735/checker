package checker.rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.Person;
public class NameRule implements ICheckRule<Person>{

	Pattern pattern = Pattern.compile("[!@#$%^&*(){}+-/.?';:|_+?><1234567890]");
	
	public CheckResult checkRule(Person entity) {

		
		if(entity.getFirstName()==null)
			return new CheckResult("", RuleResult.Error);
		if(entity.getFirstName().equals(""))
			return new CheckResult("", RuleResult.Error);
		
		Matcher matcher = pattern.matcher(entity.getFirstName());
		if(matcher.find()) 
			return new CheckResult("", RuleResult.Error);
		return new CheckResult("", RuleResult.Ok);
	}


}
