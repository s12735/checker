package checker.rules;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import checker.CheckResult;
import checker.ICheckRule;
import checker.RuleResult;
import domain.User;

public class PasswordRule implements ICheckRule<User> {
	private Pattern pattern;
	private Matcher matcher;
	private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20})";

	public CheckResult checkRule(User entity) {
		if (Validate(entity.getPassword()) == false) {
			return new CheckResult("", RuleResult.Error);
		}

		return new CheckResult("", RuleResult.Ok);
	}

	public boolean Validate(String password) {
		pattern = Pattern.compile(PASSWORD_PATTERN);
		matcher = pattern.matcher(password);
		return matcher.matches();
	}
}
