package checker.roles.tests;

import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.DateOfBirthRule;
import domain.Person;

public class DateOfBirthTest {
	SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	DateOfBirthRule rule = new DateOfBirthRule();

	@Test
	public void check_if_date_is_the_same_in_pesel() throws ParseException{
	Date data = df.parse("18/10/1995");  // date in format dd/MM/yyyy
	Person p = new Person();
	p.setPesel("95101803441");			// pesel
	p.setDateOfBirth(data);
	CheckResult result = rule.checkRule(p);
	assertTrue(result.getResult().equals(RuleResult.Ok));
	}
}
