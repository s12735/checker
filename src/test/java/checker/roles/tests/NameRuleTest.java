package checker.roles.tests;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.NameRule;
import domain.Person;

public class NameRuleTest {
	
	NameRule rule = new NameRule();
	
	@Test
	public void check_if_the_person_name_is_not_null(){
		Person p = new Person();
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void check_if_the_person_name_is_not_empty(){
		Person p = new Person();
		p.setFirstName("");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
	@Test
	public void if_the_name_is_not_null_should_return_OK(){
		Person p = new Person();
		p.setFirstName("Jan");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
		
	}
	
	@Test
	public void if_the_name_not_contains_special_char(){
		Person p = new Person();		
		p.setFirstName("Jan1");
		CheckResult result =rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
		
	}
	
}
