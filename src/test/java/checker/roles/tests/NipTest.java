package checker.roles.tests;

import org.junit.Test;

import checker.CheckResult;
import checker.RuleResult;
import checker.rules.NipRule;
import domain.Person;
import static org.junit.Assert.*;

public class NipTest {
	NipRule rule = new NipRule();
	
	@Test
	public void check_if_nip_if_correct(){
		Person p = new Person();
		p.setNip("9492107026"); // correct nip
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}
	
	@Test
	public void check_if_nip_if_correct_sholud_return_false(){
		Person p = new Person();
		p.setNip("1234567890"); // wrong nip
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
}
