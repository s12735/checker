package checker.roles.tests;

import org.junit.Test;
import static org.junit.Assert.*;
import checker.CheckResult;
import checker.RuleResult;
import checker.rules.PeselRule;
import domain.Person;

public class PeselTest {
	PeselRule rule = new PeselRule();
	
	@Test
	public void check_if_pesel_is_set(){
		Person p = new Person();
		p.setPesel(null);
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Error));
	}
	
	@Test
	public void check_if_lenght_is_11(){
		Person p = new Person();
		p.setPesel("95110205856");
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}
	
	@Test
	public void check_if_pesel_is_vaild(){
		Person p = new Person();
		p.setPesel("95110205856"); // poprawny pesel
		CheckResult result = rule.checkRule(p);
		assertTrue(result.getResult().equals(RuleResult.Ok));
	}
	
}
